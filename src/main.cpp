#include <Arduino.h>
#include <PWMServo.h>

PWMServo yaw;
PWMServo pitch;

double YAW_MAX = 7;
double PITCH_MAX = 7;

double zero_height;

float read_float();
float calc_height(float yaw, float pitch);
float yaw_conv(float theta);
float pitch_conv(float theta);

void setup() {
  delay(5000);
  yaw.attach(29);   // CHANGE MEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
  pitch.attach(33); // CHANGE MEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE

  Serial.println("Remove Servo Horns and press enter.");
  read_float();
  yaw.write(90);
  pitch.write(90);

  Serial.println("Replace horns, level the TVC and press enter.");
  read_float();

  Serial.println("Testing Servos");

  // Do a full range of movement of the servos
  for (double pct = -100.0; pct <= 100.0; pct += 10.0) {
    Serial.print(".");
    double yaw_pos = (pct / 100) * YAW_MAX;
    double pitch_pos = (pct / 100) * PITCH_MAX;

    yaw.write(90 + yaw_conv(yaw_pos));
    pitch.write(90 + pitch_conv(pitch_pos));
    delay(25);
  }

  yaw.write(90);
  pitch.write(90);

  Serial.print("Please input the point height in millimeters: ");
  zero_height = read_float();
  delay(1000);
}

void loop() {
  Serial.print("Enter Yaw Coord: ");
  float yaw_pos = read_float();
  if (abs(yaw_pos) > YAW_MAX) {
    Serial.println("Value too large, defaulting to maximum deflection");
    yaw_pos = YAW_MAX;
  }
  Serial.println();
  Serial.println();

  Serial.print("Enter Pitch Coord: ");
  float pitch_pos = read_float();
  if (abs(pitch_pos) > PITCH_MAX) {
    Serial.println("Value too large, defaulting to maximum deflection");
    pitch_pos = PITCH_MAX;
  }
  Serial.println();
  Serial.println();

  Serial.println("Moving servos to [" + String(yaw_pos) + ", " +
                 String(pitch_pos) + "]");

  yaw.write(90 + yaw_conv(yaw_pos));
  pitch.write(90 + pitch_conv(pitch_pos));

  Serial.print("Calculating current height");
  for (int i = 0; i < 5; i++) {
    Serial.print(".");
    delay(i * 100);
  }
  Serial.println("");

  float height =
      235 * (sin(yaw_pos * 3.14 / 180) + sin(pitch_pos * 3.14 / 180)) +
      zero_height;

  Serial.println("Please verify current height is correct: " + String(height) +
                 "\n");
  Serial.println("Press enter to continue.");
  read_float();
}

float read_float() {
  char line[80];
  int count = 0;

  while (1) {
    if (Serial.available() > 0) {
      line[count] = (char)Serial.read(); // store the char
      Serial.print(line[count]);
      if (line[count++] == '\r') { // if its a CR,
        line[count] = '\0';        //  zero-terminate it
        return String(line).toFloat();
      }
    }
  }
}

float yaw_conv(float thetad) {
  // Serial.println(thetad);
  float h = .2875;
  float H = 1.6;

  float theta = thetad * 3.14 / 180;

  return -(2 * asin((H * sin(theta / 2)) / h)) * 180.0 / 3.14;
}
float pitch_conv(float thetad) {
  // Serial.println(thetad);
  float h = .2875;
  float H = 1.2;

  float theta = thetad * 3.14 / 180;

  return -(2 * asin((H * sin(theta / 2)) / h)) * 180.0 / 3.14;
}
