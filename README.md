# TVC Test

The purpose of this test is to demonstrate the capabilities of the TVC system. The TVC is mechanically very complex, and it is vital that the servo movements accurately rotate the rocket motor to the correct angle. This test will verify requirements for the Test Software, Test Stand, and Control Mechanisms Subsystem.